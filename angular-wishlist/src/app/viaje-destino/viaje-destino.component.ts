import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Destinoviaje } from '../models/destino-viaje.model';


@Component({
  selector: 'app-viaje-destino',
  templateUrl: './viaje-destino.component.html',
  styleUrls: ['./viaje-destino.component.css']
})
export class ViajeDestinoComponent implements OnInit {
  @Input() destino: Destinoviaje;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() {}

  ngOnInit(): void {
  }

}
