import { Component, OnInit } from '@angular/core';
import { fakeAsync } from '@angular/core/testing';
import { Destinoviaje } from './../models/destino-viaje.model';
@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
export class ListaDestinoComponent implements OnInit {
  destinos: Destinoviaje [];
  constructor() {
    this.destinos = [];
   }

  ngOnInit(): void {
  }
  guardar(nombre:string, url:string):boolean {
    this.destinos.push (new Destinoviaje (nombre, url));
    console.log (this.destinos);
    return false;
  }
}
